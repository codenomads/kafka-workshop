package nl.workshop.kafka.testing;

import static org.assertj.core.api.Assertions.assertThat;
import nl.workshop.kafka.testing.consumer.SpringMessageConsumer;
import nl.workshop.kafka.testing.model.Message;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.kafka.config.KafkaListenerEndpointRegistry;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.listener.MessageListenerContainer;
import org.springframework.kafka.support.serializer.JsonSerializer;
import org.springframework.kafka.test.utils.ContainerTestUtils;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.utility.DockerImageName;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.concurrent.TimeUnit;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@ContextConfiguration(initializers = SpringMessageConsumerTestContainerTestsIT.Initializer.class)
class SpringMessageConsumerTestContainerTestsIT {
  private static final Logger LOG = LoggerFactory.getLogger(SpringMessageProducerTestContainerTestsIT.class);

  public static KafkaContainer kafka = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:5.3.1"));

  static {  kafka.start();}

  @Autowired
  private SpringMessageConsumer messageConsumer;

  @Autowired
  private KafkaListenerEndpointRegistry kafkaListenerEndpointRegistry;

  private KafkaTemplate<String, Message> template;


  @BeforeEach
  public void setUp() {

    Map<String, Object> producerProperties =
        KafkaTestUtils.producerProps(
            kafka.getBootstrapServers());


    producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
    producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);

    ProducerFactory<String, Message> producerFactory =
        new DefaultKafkaProducerFactory<String, Message>(
            producerProperties);

    template = new KafkaTemplate<>(producerFactory);
    // set the default topic to send to
    template.setDefaultTopic("messages");

    for (MessageListenerContainer messageListenerContainer : kafkaListenerEndpointRegistry
        .getListenerContainers()) {
      ContainerTestUtils.waitForAssignment(messageListenerContainer,
          1);
    }

  }


  @DisplayName("It should consume messages from the messages topic")
  @Test
  void test_receiveMessage() throws InterruptedException {
    Message message = new Message(LocalDateTime.now(), "Hi everybody");
    template.sendDefault(message);

    messageConsumer.getLatch().await(10000, TimeUnit.MILLISECONDS);
    assertThat(messageConsumer.getLatch().getCount()).isEqualTo(0);

  }

  public static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    @Override
    public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
      TestPropertyValues values = TestPropertyValues.of(
          "spring.kafka.consumer.bootstrap-servers=" + kafka.getBootstrapServers()
      );
      values.applyTo(configurableApplicationContext);
      LOG.info("connecting test to kafka instance: {}",  kafka.getBootstrapServers());
    }
  }

}
