package nl.workshop.kafka.testing;

import static org.assertj.core.api.Assertions.assertThat;
import nl.workshop.kafka.testing.consumer.SpringMessageConsumer;
import nl.workshop.kafka.testing.model.Message;


import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.kafka.support.serializer.JsonSerializer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.config.KafkaListenerEndpointRegistry;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.listener.MessageListenerContainer;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.utils.ContainerTestUtils;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.concurrent.TimeUnit;


@EmbeddedKafka( partitions = 1, topics = {"messages"})
@ExtendWith(SpringExtension.class)

@SpringBootTest(
    properties = "spring.kafka.consumer.bootstrap-servers=${spring.embedded.kafka.brokers}"
)
class SpringMessageConsumerEmbeddedKafkaTestsIT {

  @Autowired
  private EmbeddedKafkaBroker embeddedKafkaBroker;

  @Autowired
  private SpringMessageConsumer messageConsumer;

  @Autowired
  private KafkaListenerEndpointRegistry kafkaListenerEndpointRegistry;

  private KafkaTemplate<String, Message> template;


  @BeforeEach
  public void setUp() {

    Map<String, Object> producerProperties =
        KafkaTestUtils.producerProps(
            embeddedKafkaBroker);


    producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
    producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);

    ProducerFactory<String, Message> producerFactory =
        new DefaultKafkaProducerFactory<String, Message>(
            producerProperties);

    template = new KafkaTemplate<>(producerFactory);
    // set the default topic to send to
    template.setDefaultTopic("messages");

    for (MessageListenerContainer messageListenerContainer : kafkaListenerEndpointRegistry
        .getListenerContainers()) {
      ContainerTestUtils.waitForAssignment(messageListenerContainer,
          embeddedKafkaBroker.getPartitionsPerTopic());
    }

  }


  @DisplayName("It should consume messages from the messages topic")
  @Test
  void test_receiveMessage() throws InterruptedException {
    Message message = new Message(LocalDateTime.now(), "Hi everybody");
    template.sendDefault(message);

    messageConsumer.getLatch().await(10000, TimeUnit.MILLISECONDS);
    assertThat(messageConsumer.getLatch().getCount()).isEqualTo(0);

  }

}

