package nl.workshop.kafka.testing;

import static java.util.Collections.emptySet;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import nl.workshop.kafka.testing.producer.MessageProducer;
import nl.workshop.kafka.testing.model.Message;
import nl.workshop.kafka.testing.producer.SimplePartitioner;
import org.apache.kafka.clients.producer.MockProducer;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.Cluster;
import org.apache.kafka.common.Node;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

class MessageProducerUnitTest {

  private static final String MESSAGE_TOPIC = "messages";

  private MessageProducer messageProducer;
  private MockProducer<String, Message> mockProducer;


  @DisplayName("Verify if message is sent")
  @Test
  void verifyHistoryAndMetaDataWhenMessageSend() throws ExecutionException, InterruptedException {
    //given
    mockProducer = new MockProducer<>(true, new StringSerializer(), new JsonSerializer());
    messageProducer = new MessageProducer(mockProducer);

    //when
    Future<RecordMetadata> record = messageProducer.send("key", new Message("test message"));

    //then
    assertThat(mockProducer.history().size()).isEqualTo(1);
    assertThat(mockProducer.history().get(0).key()).isEqualTo("key");
    assertThat(mockProducer.history().get(0).value().getText()).isEqualTo("test message");
    assertThat(mockProducer.history().get(0).topic()).isEqualTo(MESSAGE_TOPIC);
    assertThat(record.get().partition()).isEqualTo(0);
  }

  @DisplayName("Verify if message is sent to the correct partition")
  @Test
  void verifyMessageIsSentToCorrectPartition()
      throws ExecutionException, InterruptedException {
    //given
    PartitionInfo partition0 = new PartitionInfo(MESSAGE_TOPIC, 0, null, null, null);
    PartitionInfo partition1 = new PartitionInfo(MESSAGE_TOPIC, 1, null, null, null);
    List<PartitionInfo> list = new ArrayList<>();
    list.add(partition0);
    list.add(partition1);

    Cluster cluster = new Cluster("cluster", new ArrayList<Node>(), list, emptySet(), emptySet());
    this.mockProducer = new MockProducer<>(cluster, true, new SimplePartitioner(),
        new StringSerializer(), new JsonSerializer());

    messageProducer = new MessageProducer(mockProducer);

    //when
    Future<RecordMetadata> recordP0 = messageProducer.send("Bkey", new Message("test message"));
    Future<RecordMetadata> recordP1 = messageProducer.send("Akey", new Message("test message"));

    //then
    assertThat(recordP0.get().partition()).isEqualTo(0);
    assertThat(recordP1.get().partition()).isEqualTo(1);
  }

  @DisplayName("Verify if Exception is thrown by the producer")
  @Test
  void verifyThrowException() {
    //given
    mockProducer = new MockProducer<>(false, new StringSerializer(), new JsonSerializer());
    messageProducer = new MessageProducer(mockProducer);

    //when
    Future<RecordMetadata> record = messageProducer.send("key", new Message("test message"));
    RuntimeException e = new RuntimeException();
    mockProducer.errorNext(e);

    //then
    try {
      record.get();
    } catch (ExecutionException | InterruptedException ex) {
      assertEquals(e, ex.getCause());
    }
    assertTrue(record.isDone());
  }

}

