package nl.workshop.kafka.testing.producer;

import nl.workshop.kafka.testing.model.Message;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;


@Service
public class SpringMessageProducer {

    private final KafkaTemplate<String, Message> kafkaTemplate;
    private static final String TOPIC = "messages";


    public SpringMessageProducer(KafkaTemplate<String, Message> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void sendMessage(Message message) {
        kafkaTemplate.send(TOPIC, "key", message);
    }

}