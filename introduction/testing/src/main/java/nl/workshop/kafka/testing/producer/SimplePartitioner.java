package nl.workshop.kafka.testing.producer;

import org.apache.kafka.clients.producer.Partitioner;
import org.apache.kafka.common.Cluster;

import java.util.Map;

public class SimplePartitioner implements Partitioner {
    @Override
    public int partition(String topic, Object key, byte[] keyBytes, Object value, byte[] valueBytes, Cluster cluster) {
        final Integer partitionCount = cluster.partitionCountForTopic(topic);
        // In our case we're using the string representation of the object to partition,
        // based on the value of the first character code.
        return key.toString().charAt(0) % partitionCount;
    }

    @Override
    public void close() {

    }

    @Override
    public void configure(Map<String, ?> map) {

    }
}
