package nl.workshop.kafka.testing.producer;

import nl.workshop.kafka.testing.model.Message;
import org.apache.kafka.clients.producer.*;

import java.util.concurrent.Future;


public class MessageProducer {

    private static final String MESSAGE_TOPIC = "messages";

    private final Producer<String, Message> producer;

    public MessageProducer(Producer<String, Message> producer) {
      this.producer = producer;
    }

    public Future<RecordMetadata> send(String key, Message value) {
      ProducerRecord record = new ProducerRecord(MESSAGE_TOPIC,
          key, value);
      return producer.send(record);
    }

    public void flush() {
      producer.flush();
    }

}

