# Exercise 3: Kafka test container Test SpringMessageProducer

In this exercise we will create another integration test for the SpringMessageProducer
Instead of using embedded kafka we will be using a kafka test container.



1. Copy the content of SpringMessageProducerEmbeddedKafkaTestsIT to SpringMessageProducerTestContainerTestsIT
2. Change the configuration, so we use a test container instead of embeddedkafka. Use SpringMessageConsumerTestContainerTestsIT as inspiration.