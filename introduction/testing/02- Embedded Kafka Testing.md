# Exercise 2: Embedded kafka test SpringMessageConsumer 

In this exercise we will create a test to test the SpringMessageConsumer 

1. First step is to add the @EmbeddedKafka annotation with some properties. Set the partitions to how many partions you would like to haveand configure the topic.
2. add @ExtendWith(SpringExtension.class) 
3. add @SpringBootTest annotation to set up the context and make sure spring.kafka.consumer.bootstrap-servers is configured to use the embedded kafka broker.
4. Autowire the EmbeddedKafkaBroker, SpringMessageConsumer, KafkaListenerEndpointRegistry
5. declare  a KafkaTemplate to send Messages.
6. In the setUp() step we are going to set up a producer to send messages we can consume in the test. 
 we can KafkaTestUtils.producerProps to configure the producer properties. set the broker to embeddedKafkaBroker, set for the key a StringSerializer and value JsonSerializer.
 Then initialize a producerFactory with created producer props. Use producerFactory to initialize the kafkaTemplate and on the template setDefaultTopic to "messages'.
 Last thing you have to  in the setup() step is to set up the listener.
 ```
     for (MessageListenerContainer messageListenerContainer : kafkaListenerEndpointRegistry
         .getListenerContainers()) {
       ContainerTestUtils.waitForAssignment(messageListenerContainer,
           embeddedKafkaBroker.getPartitionsPerTopic());
     }
 ```
7. Now we are ready to write the test. create a message  and use the kafka template to send the message.
8. Then get the latch from the messageConsumer  await 10 seconds to avoid timing issues);
9. Finally  assertThat(messageConsumer.getLatch().getCount()).isEqualTo(0);