# Exercise 1: Mock Consumer 

In this exercise you can follow this tutorial: https://www.baeldung.com/kafka-mockconsumer
We will be implementing the same test for our MessageConsumer in the file MessageConsumerUnitTest.

1. Declare our messageConsumer & mockConsumer, also declare a message list to collect the messages sent and a pollingException.
2. In the Before step initialize the mock,  messageConsumer and the message list as empty list. (see example step 3.1. Creating a MockConsumer Instance
from blogpost)
3. Now we are ready to implement the test we will start with the happy scenario, to verify if messages are consumed correctly
Before we can consume a message we have to do some preperations in the // given step  
Because we cannot add records before assigning or subscribing to a topic we have to use the MockConsumer schedulePollTask.
We start with a schedulePollTask to rebalance and then add a record to the mockConsumer using the addRecord method.
We also have to configure the beginning offsets in the mockconsumer.
(see example step 3.3. Subscribing to Topics from the blogpost) 
4. In the // when step we call the method we would like to test messageConsumer.consume()
5. In the // then  we will check if the message is added to the messages list and if the consumer is closed. 
6. Run the test to check if its succeeding
7. Now we will implement a second test with a mock which is throwing a exception.
8. in the // given step  we schedule a poll task to set an exception to be thrown using the setPollException instead of adding a record. the rest the  given step is the same as
the happy flow. (see example 3.4. Controlling the Polling Loop).  
9. In the // when step we call the method we would like to test again  messageConsumer.consume()
10. In the // then  we will check now if a exception is thrown and if the consumer is closed. 
11. Run the test to check if its succeeding