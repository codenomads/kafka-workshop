# Exercise 2: Developing a Kafka Producer

1. Read the javadoc of [KafkaProducer](https://kafka.apache.org/26/javadoc/org/apache/kafka/clients/producer/KafkaProducer.html) to know how to use the Producer API (to send messages to Kafka)
2. Create a new Java Maven project
3. Define dependency for Kafka Clients API library
    1. Use [mvnrepository](https://mvnrepository.com/artifact/org.apache.kafka/kafka-clients/2.6.0) to know the proper entry for `kafka-clients` dependency
4. Write the code of a Kafka producer
    1. Start with an empty `Properties` object and fill out the missing properties as per the documentation or runtime exceptions
    2. Use [ProducerConfig](https://kafka.apache.org/26/javadoc/org/apache/kafka/clients/producer/ProducerConfig.html) constants (not string values for properties)
    3. Don't forget to `close` the producer (so the messages are actually sent out to the broker)
5. Run the producer
    1. Use the consumer from exercise 1 to receive the messages
