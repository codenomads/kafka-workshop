package nl.codenomads.training.kafka.transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;

@Service
public class ProducerApplicationWithTransactionalBean {

    private final TransactionalProducer producer;

    @Autowired
    public ProducerApplicationWithTransactionalBean(TransactionalProducer producer) {
        this.producer = producer;
    }

    public void run() {
        try {
            while (true) {
                producer.sendMessages();
            }
        } catch (ShutdownProducerException ex) {
            // suppress the exception - just to demonstrate @Transactional support on beans
            return;
        }
    }

}
