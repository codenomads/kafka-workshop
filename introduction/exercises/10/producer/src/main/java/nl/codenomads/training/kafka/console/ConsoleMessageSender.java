package nl.codenomads.training.kafka.console;

import nl.codenomads.training.kafka.Message;
import nl.codenomads.training.kafka.transactional.ShutdownProducerException;
import org.springframework.data.util.Pair;
import org.springframework.kafka.core.KafkaOperations;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.Scanner;

@Component
public class ConsoleMessageSender {

    private final Scanner IN = new Scanner(System.in);

    public boolean promptAndSendMessage(KafkaOperations<String, Message> template) {

        System.out.print("Receiver: ");
        final String receiver = IN.nextLine();

        System.out.print("Message: ");
        final String message = IN.nextLine();

        if (message.isEmpty()) {
            return false;
        }

        template.send(Message.TOPIC, receiver, new Message(message))
                .addCallback(
                        result -> System.out.printf(" * Record sent to partition %d with offset %d * ",
                                result.getRecordMetadata().partition(), result.getRecordMetadata().offset()),
                        System.err::println);

        return true;
    }

}
