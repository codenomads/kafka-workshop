package nl.codenomads.training.kafka.template;

import nl.codenomads.training.kafka.Message;
import nl.codenomads.training.kafka.console.ConsoleMessageSender;
import nl.codenomads.training.kafka.transactional.ShutdownProducerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class ProducerApplicationWithTemplateMethod {

    private final KafkaTemplate<String, Message> kafkaTemplate;
    private final ConsoleMessageSender messageSender;

    @Autowired
    public ProducerApplicationWithTemplateMethod(KafkaTemplate<String, Message> kafkaTemplate,
                                                 ConsoleMessageSender messageSender) {
        this.kafkaTemplate = kafkaTemplate;
        this.messageSender = messageSender;
    }

    public void run() {

        while (
            kafkaTemplate.executeInTransaction(ops -> {
                final boolean messagesWasSent =
                        messageSender.promptAndSendMessage(ops) &&
                        messageSender.promptAndSendMessage(ops);
                if (!messagesWasSent) {
                    throw new ShutdownProducerException();
                }
                return true;
            }
        ));

    }

}
