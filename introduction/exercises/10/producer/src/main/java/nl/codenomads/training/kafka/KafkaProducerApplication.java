package nl.codenomads.training.kafka;

import nl.codenomads.training.kafka.template.ProducerApplicationWithTemplateMethod;
import nl.codenomads.training.kafka.transactional.ProducerApplicationWithTransactionalBean;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;

@SpringBootApplication
@EnableKafka
public class KafkaProducerApplication implements ApplicationRunner {

    private final ProducerApplicationWithTransactionalBean producerWithBean;
    private final ProducerApplicationWithTemplateMethod producerWithTemplate;

    public KafkaProducerApplication(ProducerApplicationWithTransactionalBean producerWithBean,
                                    ProducerApplicationWithTemplateMethod producerWithTemplate) {
        this.producerWithBean = producerWithBean;
        this.producerWithTemplate = producerWithTemplate;
    }

    @Override
    public void run(ApplicationArguments args) {
        //producerWithBean.run();
        producerWithTemplate.run();
    }

    public static void main(String[] args) {
        SpringApplication.run(KafkaProducerApplication.class, args);
    }
}
