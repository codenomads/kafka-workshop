package nl.codenomads.training.kafka.transactional;

import nl.codenomads.training.kafka.Message;
import nl.codenomads.training.kafka.console.ConsoleMessageSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class TransactionalProducer {

    private final KafkaTemplate<String, Message> kafkaTemplate;
    private final ConsoleMessageSender messageSender;

    @Autowired
    public TransactionalProducer(KafkaTemplate<String, Message> kafkaTemplate,
                                 ConsoleMessageSender messageSender) {
        this.kafkaTemplate = kafkaTemplate;
        this.messageSender = messageSender;
    }

    @Transactional
    public void sendMessages() {
        final boolean messagesWasSent =
                messageSender.promptAndSendMessage(kafkaTemplate) &&
                messageSender.promptAndSendMessage(kafkaTemplate);
        if (!messagesWasSent) {
            throw new ShutdownProducerException();
        }
    }

}
