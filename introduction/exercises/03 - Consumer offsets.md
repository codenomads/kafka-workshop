# Exercise 3: Offsets in Kafka Consumer

1. Go back to the javadoc of [KafkaConsumer](https://kafka.apache.org/26/javadoc/org/apache/kafka/clients/consumer/KafkaConsumer.html) to know how to switch between manual and automatic offset committing.
2. Make sure the offset of each message is printed on screen so you can keep track of the consumer behaviour.
3. Automatic offset commits:
    1. Ensure that auto commit is in use by the consumer.
    2. Simulate a fatal failure after reading 3 messages. For example, throw a runtime exception when reading the 4th message.
    3. Restart the consumer.
    4. What's the offset of the first message it receives?
4. Manual offset commits:
    1. Ensure that manual commit is in use by the consumer.
    2. Simulate a fatal failure after reading 3 messages. For example, throw a runtime exception when reading the 4th message.
    3. Restart the consumer.
    4. What's the offset of the first message it receives?
5. What advantages and disadvantages can you find from each method?
