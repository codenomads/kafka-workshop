# Exercise 1: Developing a Kafka Consumer

1. Read the javadoc of [KafkaConsumer](https://kafka.apache.org/26/javadoc/org/apache/kafka/clients/consumer/KafkaConsumer.html) to know how to use the Consumer API (to consume messages from Kafka)
2. Create a new Java Maven project
3. Define dependency for Kafka Clients API library
    1. Use [mvnrepository](https://mvnrepository.com/artifact/org.apache.kafka/kafka-clients/2.6.0) to know the proper entry for `kafka-clients` dependency
4. Write a Kafka consumer
    1. Start with an empty `Properties` object and fill out the missing properties as per the documentation or runtime exceptions
    2. Use [ConsumerConfig](https://kafka.apache.org/26/javadoc/org/apache/kafka/clients/consumer/ConsumerConfig.html) constants (not string values for properties)
    3. Don't forget to `close` the consumer (so the messages are actually acknowledged to the broker)
5. Run the Kafka consumer
    1. Use `kafka-console-producer` to produce messages

