# Exercise 5: Consumer groups

1. Ensure that your consumer properties contain an entry for the consumer group name.
2. Run three instances of the consumer.
3. Use the producer to send messages to each partition.
4. How did the consumers behave?
