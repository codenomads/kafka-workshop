package nl.codenomads.training.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaListener;

@SpringBootApplication
@EnableKafka
public class KafkaConsumerApplication implements ApplicationRunner {

    @KafkaListener(topics = Message.TOPIC)
    public void onMessage(ConsumerRecord<String, Message> record) {
        System.out.printf("partition = %d, offset = %d, key = %s%n value = %s%n%n",
                record.partition(), record.offset(), record.key(), record.value());
    }

    @Override
    public void run(ApplicationArguments args) {
        System.out.println("Use CTRL+C to exit...");
    }

    public static void main(String[] args) {
        SpringApplication.run(KafkaConsumerApplication.class, args);
    }

}
