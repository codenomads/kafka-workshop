package nl.codenomads.training.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.Scanner;

@SpringBootApplication
@EnableKafka
public class KafkaProducerApplication implements ApplicationRunner {

    @Autowired
    private KafkaTemplate<String, Message> kafkaTemplate;

    @Override
    public void run(ApplicationArguments args) {

        final Scanner in = new Scanner(System.in);
        while (true) {
            System.out.print("Receiver: ");
            final String receiver = in.nextLine();
            System.out.print("Message: ");
            final String message = in.nextLine();
            if (!message.isEmpty()) {
                kafkaTemplate.send(Message.TOPIC, receiver, new Message(message))
                        .addCallback(
                                result -> System.out.printf(" * Record sent to partition %d with offset %d * ",
                                        result.getRecordMetadata().partition(), result.getRecordMetadata().offset()),
                                System.err::println);
            } else {
                break;
            }
        }
    }

    public static void main(String[] args) {
        SpringApplication.run(KafkaProducerApplication.class, args);
    }
}
