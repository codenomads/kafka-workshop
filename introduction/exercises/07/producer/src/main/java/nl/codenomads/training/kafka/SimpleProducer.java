package nl.codenomads.training.kafka;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;
import java.util.Scanner;

public class SimpleProducer {

    private static final String MESSAGE_TOPIC = "messages";

    public static void main(String[] args) {
        final Scanner in = new Scanner(System.in);

        final Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(ProducerConfig.ACKS_CONFIG, "all");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonMessageSerializer.class.getName());
        props.put(ProducerConfig.PARTITIONER_CLASS_CONFIG, SimplePartitioner.class.getName());

        try (final Producer<String, Message> producer = new KafkaProducer<>(props)) {

            while (true) {
                System.out.print("Receiver: ");
                final String receiver = in.nextLine();
                System.out.print("Message: ");
                final String message = in.nextLine();
                if (!message.isEmpty()) {
                    producer.send(new ProducerRecord<>(MESSAGE_TOPIC, receiver, new Message(message)),
                            SimpleProducer::onMessageSent);
                } else {
                    break;
                }
            }

        }
    }

    private static void onMessageSent(RecordMetadata metadata, Exception e) {
        if (null != e) {
            e.printStackTrace();
        } else {
            System.out.printf(" * Record sent to partition %d with offset %d * ",
                    metadata.partition(), metadata.offset());
        }
    }

}
