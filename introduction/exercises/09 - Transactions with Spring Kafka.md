# Exercise 9: Transactions with Spring Kafka

1. Replicate the transactional producer from the Kafka API exercise, but using the Spring Kafka framework.
    1. You may choose to use the `@Transactional` annotation or the `KafkaTemplate::executeInTransaction` method.
    2. Remember that both only abort a transaction when an exception is thrown.
