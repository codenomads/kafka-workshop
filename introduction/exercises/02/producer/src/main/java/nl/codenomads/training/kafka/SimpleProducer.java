package nl.codenomads.training.kafka;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;
import java.util.Scanner;

public class SimpleProducer {

    public static void main(String[] args) {
        final Scanner in = new Scanner(System.in);

        // TODO: create and configure the producer

        while (true) {
            System.out.print("Key: ");
            final String key = in.nextLine();
            System.out.print("Message: ");
            final String message = in.nextLine();
            if (!message.isEmpty()) {

                // TODO: send the message to Kafka

            } else {
                break;
            }
        }

        // Don't forget to close the producer!

    }

}
