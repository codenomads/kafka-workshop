# Exercise 8: Spring Kafka with Spring Boot

Tip: Take a look at the [Spring Boot with Kafka](https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-kafka) documentation.

1. Duplicate your `Message` class, serializer and deserializer in the skeleton, in the correct producer and consumer projects.
2. Enable support for Kafka in both Spring Boot producer and consumer projects (tip: see the [EnableKafka](https://docs.spring.io/spring-kafka/api/org/springframework/kafka/annotation/EnableKafka.html) annotation).
3. For the producer:
    1. Set the Kafka configuration properties in the `application.yml` file.
    2. Inject an instance of [KafkaTemplate](https://docs.spring.io/spring-kafka/api/org/springframework/kafka/core/KafkaTemplate.html).
    3. Use it just like you used your previous `KafkaProducer`.
4. For the consumer:
    1. Set the Kafka configuration properties in the `application.yml` file.
    2. Take advantage of the [@KafkaListener](https://docs.spring.io/spring-kafka/reference/html/#kafka-listener-annotation) annotation to listen to messages from a topic.
5. Run the consumer and the producer.
    1. Verify that the consumer reads and decodes incoming messages correctly.
6. JSON (de)serialization out of the box:
    1. Replace your JSON serializer with the [JsonDeserializer](https://docs.spring.io/spring-kafka/api/org/springframework/kafka/support/serializer/JsonDeserializer.html) provided by the Spring Kafka framework.
    2. Replace your JSON deserializer with the [JsonSerializer](https://docs.spring.io/spring-kafka/api/org/springframework/kafka/support/serializer/JsonSerializer.html) provided by the Spring Kafka framework.
    3. You may need to adjust the `spring.kafka.consumer.properties.spring.json.trusted.packages` property in the consumer.
    4. Try the new (de)serializers.
