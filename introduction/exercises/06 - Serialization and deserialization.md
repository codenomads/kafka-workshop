# Exercise 6: Serialization and deserialization

1. Write a POJO to serve as DTO with two properties that will be used as the message value. For example, a timestamp and a message.
2. Implement a custom `Serializer` for the POJO.
    1. Read about the (Serializer)[http://kafka.apache.org/26/javadoc/org/apache/kafka/common/serialization/Serializer.html] interface.
    2. Serialize the new POJO to JSON. Use the serialization library of your choosing (suggestion: Jackson).
3. Register this serializer in the existing producer.
    1. You can test serialization already. Produce messages and read them with the existing consumer, they should be printed as JSON strings.
4. Implement a custom `Deserializer` for the POJO.
    1. Read about the (Deserializer)[http://kafka.apache.org/26/javadoc/org/apache/kafka/common/serialization/Deserializer.html] interface.
    2. Convert the incoming JSON into the POJO.
5. Register this deserializer in the existing consumer.
    1. Receive messages of POJO type instead of String.
    2. Print the incoming POJO contents for each message.
6. Run the consumer and the producer.
    1. Verify that the consumer reads and decodes incoming messages correctly.
