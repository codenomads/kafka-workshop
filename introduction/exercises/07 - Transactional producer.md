# Exercise 7: Transactional producer

1. Change the producer so that it is transactional. See the documentation for the (KafkaProducer)[https://kafka.apache.org/26/javadoc/org/apache/kafka/clients/producer/KafkaProducer.html] for a sample and usage instructions.
2. First, send one message at a time like before. Verify with the consumer that it works.
3. Now send multiple messages in one transaction. For example, commit only every two messages.
4. Specify the isolation level in the consumer.
    1. See the behaviour with `read-committed`.
    2. See the behaviour with `read-uncommitted`.
