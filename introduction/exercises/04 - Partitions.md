# Exercise 4: Partitions

1. Use `kafka-topics --alter` to change the existing topic to have two partitions.
2. Write a custom `Partitioner`
    1. Implement [Partitioner](https://kafka.apache.org/26/javadoc/org/apache/kafka/clients/producer/Partitioner.html) interface
    2. Review [Cluster](https://kafka.apache.org/26/javadoc/org/apache/kafka/common/Cluster.html) (which is one of the input arguments of `Partitioner.partition` method)
    3. Decide what to do when requested for a partition ID in `partition` method
2. Update the KafkaProducer
    1. Register the custom `Partitioner` using [ProducerConfig.PARTITIONER_CLASS_CONFIG](https://kafka.apache.org/26/javadoc/org/apache/kafka/clients/producer/ProducerConfig.html#PARTITIONER_CLASS_CONFIG) property
    2. Use a `Callback` input object (to `Producer.send`) and display the partition ID
3. Start one consumer:
    1. Use the producer to send messages to different partitions on the topic (as per your decision on 2.3) and note the pertition number.
    2. Note the partition number of messages received by the consumer.
4. Start a second consumer:
    1. Use the producer to send messages to different partitions on the topic (as per your decision on 2.3) and note the pertition number.
    2. Note the partition number of messages received by each consumer.
