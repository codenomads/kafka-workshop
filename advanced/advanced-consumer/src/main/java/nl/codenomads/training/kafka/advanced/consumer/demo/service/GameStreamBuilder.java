package nl.codenomads.training.kafka.advanced.consumer.demo.service;

import nl.codenomads.training.kafka.advanced.consumer.demo.model.AnswerCorrected;
import nl.codenomads.training.kafka.advanced.consumer.demo.model.PlayerAnswer;
import nl.codenomads.training.kafka.advanced.consumer.demo.model.PlayerAnswerCorrected;
import nl.codenomads.training.kafka.advanced.consumer.demo.model.PlayerScore;
import nl.codenomads.training.kafka.advanced.consumer.demo.model.Question;
import nl.codenomads.training.kafka.advancedproducer.model.Player;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.GlobalKTable;
import org.apache.kafka.streams.kstream.Grouped;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.state.KeyValueStore;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.support.serializer.JsonSerde;
import org.springframework.stereotype.Component;
import java.time.Duration;

@Component
public class GameStreamBuilder {

	@Bean
	public KStream<String, PlayerScore> constructGame(StreamsBuilder streamsBuilder, @Value("${kafka.topic.player-answer}") String playerAnswerTopic,
			@Value("${kafka.topic.question}") String questionTopic, @Value("${kafka.topic.player}") String playerTopic,
			@Value("${kafka.topic.player-score}") String playerScoreTopic) {
		Serde<String> stringSerde = Serdes.String();
		JsonSerde<PlayerAnswer> playerAnswerSerde = new JsonSerde<>(PlayerAnswer.class);
		JsonSerde<Question> questionSerde = new JsonSerde<>(Question.class);
		JsonSerde<Player> playerSerde = new JsonSerde<>(Player.class);
		JsonSerde<PlayerAnswerCorrected> playerAnswerCorrectedSerde = new JsonSerde<>(PlayerAnswerCorrected.class);
		JsonSerde<PlayerScore> playerScoreSerde = new JsonSerde<>(PlayerScore.class);

		GlobalKTable<String, Question> questionTable = streamsBuilder.globalTable(questionTopic, Consumed.with(stringSerde, questionSerde));
		GlobalKTable<String, Player> playerTable = streamsBuilder.globalTable(playerTopic, Consumed.with(stringSerde, playerSerde));

		KStream<String, PlayerScore> playerAnswerStream = streamsBuilder
				.stream(playerAnswerTopic, Consumed.with(stringSerde, playerAnswerSerde))
				.peek(this::print)
				.join(questionTable, (key, value) -> key, this::checkAnswer)
				.filter((key, answerCorrected) -> answerCorrected.isAnswerCorrect())
				.selectKey((key, answerCorrected) -> answerCorrected.getPlayerId())
				.join(playerTable, (key, value) -> key, this::addPlayerName)
				.groupByKey(Grouped.with(stringSerde, playerAnswerCorrectedSerde))
				.aggregate(PlayerScore::new, (aggKey, answerCorrected, playerScore) ->
								addCorrectedAnswerToScore(answerCorrected, playerScore),
						Materialized.<String, PlayerScore, KeyValueStore<Bytes, byte[]>>as("player-score-store")
								.withKeySerde(stringSerde)
								.withValueSerde(playerScoreSerde)
								.withRetention(Duration.ofDays(14)))
				.toStream();

		playerAnswerStream.to(playerScoreTopic, Produced.with(stringSerde, playerScoreSerde));

		return playerAnswerStream;
	}

	private void print(String key, PlayerAnswer playerAnswer) {
		System.out.println("key: " + key + "\n value: " + playerAnswer.toString());
	}

	private AnswerCorrected checkAnswer(PlayerAnswer playerAnswer, Question question) {
		AnswerCorrected answerCorrected = new AnswerCorrected(playerAnswer.getPlayerId(), playerAnswer.getQuestionId());

		boolean answerCorrect = playerAnswer.getAnswer().equals(question.getCorrectAnswer());
		answerCorrected.setAnswerCorrect(answerCorrect);

		return answerCorrected;
	}

	private PlayerAnswerCorrected addPlayerName(AnswerCorrected answerCorrected, Player player) {
		return new PlayerAnswerCorrected(player.getId(), player.getName(), answerCorrected.getQuestionId(),
				answerCorrected.isAnswerCorrect());
	}

	private PlayerScore addCorrectedAnswerToScore(PlayerAnswerCorrected playerAnswerCorrected, PlayerScore playerScore) {
		if(playerScore.getPlayerId() == null) {
			playerScore.setPlayerId(playerAnswerCorrected.getPlayerId());
			playerScore.setPlayerName(playerAnswerCorrected.getPlayerName());
		}

		boolean newQuestion = playerScore.addQuestion(playerAnswerCorrected.getQuestionId());

		if(newQuestion) {
			playerScore.increaseScore();
		}

		return playerScore;
	}
}