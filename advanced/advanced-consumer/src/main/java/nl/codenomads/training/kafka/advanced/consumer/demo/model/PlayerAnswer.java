package nl.codenomads.training.kafka.advanced.consumer.demo.model;

public class PlayerAnswer {

	private String playerId;
	private String questionId;
	private String answer;

	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

	public String getQuestionId() {
		return questionId;
	}

	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	@Override
	public String toString() {
		return "PlayerAnswer{" +
				"playerId='" + playerId + '\'' +
				", questionId='" + questionId + '\'' +
				", answer='" + answer + '\'' +
				'}';
	}
}
