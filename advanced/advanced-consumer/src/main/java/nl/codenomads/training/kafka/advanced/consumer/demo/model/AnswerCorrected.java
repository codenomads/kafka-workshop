package nl.codenomads.training.kafka.advanced.consumer.demo.model;

public class AnswerCorrected {

	private String playerId;
	private String questionId;
	private boolean answerCorrect;

	public AnswerCorrected() {

	}

	public AnswerCorrected(String playerId, String questionId) {
		this.playerId = playerId;
		this.questionId = questionId;
	}

	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

	public String getQuestionId() {
		return questionId;
	}

	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}

	public boolean isAnswerCorrect() {
		return answerCorrect;
	}

	public void setAnswerCorrect(boolean answerCorrect) {
		this.answerCorrect = answerCorrect;
	}

	@Override
	public String toString() {
		return "AnswerCorrected{" +
				"playerId='" + playerId + '\'' +
				", questionId='" + questionId + '\'' +
				", answerCorrect=" + answerCorrect +
				'}';
	}
}
