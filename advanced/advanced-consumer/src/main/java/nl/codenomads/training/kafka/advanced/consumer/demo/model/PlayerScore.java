package nl.codenomads.training.kafka.advanced.consumer.demo.model;

import java.util.HashSet;
import java.util.Set;

public class PlayerScore {

	private String playerId;
	private String playerName;
	private Set<String> answeredQuestions = new HashSet<>();
	private int score;

	public boolean addQuestion(String questionId) {
		return answeredQuestions.add(questionId);
	}
	public void increaseScore() {
		this.score++;
	}

	public PlayerScore(){
		this.score = 0;
	}

	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public Set<String> getAnsweredQuestions() {
		return answeredQuestions;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
}
