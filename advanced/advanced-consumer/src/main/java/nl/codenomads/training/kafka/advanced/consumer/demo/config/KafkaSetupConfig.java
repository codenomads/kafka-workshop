package nl.codenomads.training.kafka.advanced.consumer.demo.config;

import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.KafkaAdmin;

@Configuration
public class KafkaSetupConfig {

	@Autowired
	private KafkaProperties kafkaProperties;

	@Value("${kafka.partitions}")
	private int partitions;

	@Value("${kafka.replicationFactor}")
	private short replicationFactor;

	@Bean
	public KafkaAdmin kafkaAdmin() {
		Map<String, Object> configs = new HashMap<>();
		configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaProperties.getBootstrapServers());
		return new KafkaAdmin(configs);
	}

	@Bean
	public NewTopic playerTopic(@Value("${kafka.topic.player}") String playerTopic) {
		return TopicBuilder
				.name(playerTopic)
				.partitions(partitions)
				.replicas(replicationFactor)
				.compact()
				.build();
	}

	@Bean
	public NewTopic questionTopic(@Value("${kafka.topic.question}") String questionTopic) {
		return TopicBuilder
				.name(questionTopic)
				.partitions(partitions)
				.replicas(replicationFactor)
				.compact()
				.build();
	}

	@Bean
	public NewTopic playerAnswerTopic(@Value("${kafka.topic.player-answer}") String playerAnswerTopic) {
		return TopicBuilder
				.name(playerAnswerTopic)
				.partitions(partitions)
				.replicas(replicationFactor)
				.build();
	}

	@Bean
	public NewTopic correctedAnswerTopic(@Value("${kafka.topic.player-score}") String playerScoreTopic) {
		return TopicBuilder
				.name(playerScoreTopic)
				.partitions(partitions)
				.replicas(replicationFactor)
				.build();
	}

	@Bean
	public NewTopic playerTopicDLT(@Value("${kafka.topic.player}") String playerTopic,
			@Value("${spring.kafka.consumer.group-id}") String consumerGroup) {
		return TopicBuilder
				.name(playerTopic + ".DLT." + consumerGroup)
				.partitions(partitions)
				.replicas(replicationFactor)
				.build();
	}
}
