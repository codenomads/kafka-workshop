package nl.codenomads.training.kafka.advanced.consumer.demo.service;

import nl.codenomads.training.kafka.advancedproducer.model.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class PlayerConsumer {

	private static final Logger LOG = LoggerFactory.getLogger(PlayerConsumer.class);

	@KafkaListener(topics = "#{'${kafka.topic.player}'}")
	public void consume(Player player)  {
		LOG.info(String.format("#### -> Consumed message -> %s", player));
	}
}
