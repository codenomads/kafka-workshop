package nl.codenomads.training.kafka.advanced.consumer.demo.model;

import java.util.List;

public class Question {

	private String id;
	private String question;
	private List<String> answers;
	private String correctAnswer;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public List<String> getAnswers() {
		return answers;
	}

	public void setAnswers(List<String> answers) {
		this.answers = answers;
	}

	public String getCorrectAnswer() {
		return correctAnswer;
	}

	public void setCorrectAnswer(String correctAnswer) {
		this.correctAnswer = correctAnswer;
	}

	@Override
	public String toString() {
		return "Question{" +
				"id='" + id + '\'' +
				", questions='" + question + '\'' +
				", answers=" + answers +
				", correctAnswer='" + correctAnswer + '\'' +
				'}';
	}
}
