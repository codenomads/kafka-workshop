package nl.codenomads.training.kafka.advanced.consumer.demo.model;

public class PlayerAnswerCorrected {

	private String playerId;
	private String playerName;
	private String questionId;
	private boolean answerCorrect;

	public PlayerAnswerCorrected() {
	}

	public PlayerAnswerCorrected(String playerId, String playerName, String questionId, boolean answerCorrect) {
		this.playerId = playerId;
		this.playerName = playerName;
		this.questionId = questionId;
		this.answerCorrect = answerCorrect;
	}

	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public String getQuestionId() {
		return questionId;
	}

	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}

	public boolean isAnswerCorrect() {
		return answerCorrect;
	}

	public void setAnswerCorrect(boolean answerCorrect) {
		this.answerCorrect = answerCorrect;
	}

	@Override
	public String toString() {
		return "PlayerAnswerCorrected{" +
				"playerId='" + playerId + '\'' +
				", playerName='" + playerName + '\'' +
				", questionId='" + questionId + '\'' +
				", answerCorrect=" + answerCorrect +
				'}';
	}
}
