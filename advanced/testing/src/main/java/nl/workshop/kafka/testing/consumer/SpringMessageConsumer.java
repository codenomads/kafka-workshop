package nl.workshop.kafka.testing.consumer;

import nl.workshop.kafka.testing.model.Message;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.concurrent.CountDownLatch;

@Service
public class SpringMessageConsumer {

    private static final Logger LOGGER =
        LoggerFactory.getLogger(SpringMessageConsumer.class);

    private static final String TOPIC = "messages";

    private CountDownLatch latch = new CountDownLatch(1);

    public CountDownLatch getLatch() {
        return latch;
    }

    @KafkaListener(topics = TOPIC)
    public void onMessage(ConsumerRecord<String, Message> record) {
        System.out.printf("partition = %d, offset = %d, key = %s%n value = %s%n%n",
            record.partition(), record.offset(), record.key(), record.value());
        latch.countDown();
    }

}