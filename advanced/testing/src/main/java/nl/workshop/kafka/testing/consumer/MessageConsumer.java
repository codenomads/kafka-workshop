package nl.workshop.kafka.testing.consumer;

import nl.workshop.kafka.testing.model.Message;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.errors.WakeupException;

import java.time.Duration;
import java.util.Collections;
import java.util.stream.StreamSupport;


public class MessageConsumer {

  private static final String MESSAGE_TOPIC = "messages";

  private Consumer<String, Message> consumer;
  private java.util.function.Consumer<Throwable> exceptionConsumer;
  private java.util.function.Consumer<Message> messageConsumer;

  public MessageConsumer(
      Consumer<String, Message> consumer, java.util.function.Consumer<Throwable> exceptionConsumer,
      java.util.function.Consumer<Message> messageConsumer) {
    this.consumer = consumer;
    this.exceptionConsumer = exceptionConsumer;
    this.messageConsumer = messageConsumer;
  }

  public void consume() {
    try {
      consumer.subscribe(Collections.singleton(MESSAGE_TOPIC));
      while (true) {
        ConsumerRecords<String, Message> records = consumer.poll(Duration.ofMillis(1000));
        StreamSupport.stream(records.spliterator(), false)
            .map(ConsumerRecord::value)
            .forEach(messageConsumer);
        consumer.commitSync();
      }
    } catch (WakeupException e) {
      System.out.println("Shutting down...");
    } catch (RuntimeException ex) {
      exceptionConsumer.accept(ex);
    } finally {
      consumer.close();
    }
  }

  public void stop() {
    consumer.wakeup();
  }

}
