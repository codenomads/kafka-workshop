package nl.workshop.kafka.testing.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import java.time.LocalDateTime;

public class Message {

    private final LocalDateTime timestamp;
    private final String text;

    public Message(String text) {
        this(LocalDateTime.now(), text);
    }

    @JsonCreator
    public Message(@JsonProperty("timestamp") LocalDateTime timestamp, @JsonProperty("text") String text) {
        this.timestamp = timestamp;
        this.text = text;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return "Message{" +
                "timestamp=" + timestamp +
                ", text='" + text + '\'' +
                '}';
    }
}
