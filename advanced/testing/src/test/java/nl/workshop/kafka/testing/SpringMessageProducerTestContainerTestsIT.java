package nl.workshop.kafka.testing;

import static org.assertj.core.api.Assertions.assertThat;
import nl.workshop.kafka.testing.model.Message;
import nl.workshop.kafka.testing.producer.SpringMessageProducer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.kafka.listener.MessageListener;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.test.utils.ContainerTestUtils;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.utility.DockerImageName;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@ContextConfiguration(initializers = SpringMessageProducerTestContainerTestsIT.Initializer.class)
class SpringMessageProducerTestContainerTestsIT {

  private static final Logger LOG = LoggerFactory.getLogger(SpringMessageProducerTestContainerTestsIT.class);

  public static KafkaContainer kafka = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:5.3.1"));

  static {  kafka.start();}

  @Autowired
  private SpringMessageProducer messageProducer;

  private KafkaMessageListenerContainer<String, Message> container;
  private BlockingQueue<ConsumerRecord<String, Message>> consumerRecords;

  @BeforeEach
  public void setUp() {
    consumerRecords = new LinkedBlockingQueue<>();

    ContainerProperties containerProperties = new ContainerProperties("messages");

    Map<String, Object> consumerProperties = KafkaTestUtils.consumerProps( kafka.getBootstrapServers(),
        "sender", "false");

    consumerProperties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
    consumerProperties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
    consumerProperties.put(JsonDeserializer.TRUSTED_PACKAGES, "nl.workshop.kafka.testing.model,java.time");

    DefaultKafkaConsumerFactory<String, Message> consumer = new DefaultKafkaConsumerFactory<>(consumerProperties);

    container = new KafkaMessageListenerContainer<>(consumer, containerProperties);
    container.setupMessageListener((MessageListener<String, Message>) record -> {
      LOG.debug("Listened message='{}'", record.toString());
      consumerRecords.add(record);
    });
    container.start();

    ContainerTestUtils.waitForAssignment(container, 1);
  }

  @DisplayName("It should send a message to the messages topic")
  @Test
  void test_sendMessage() throws InterruptedException, IOException {
    Message message = new Message(LocalDateTime.now(), "Hi everybody");
    messageProducer.sendMessage(message);

    ConsumerRecord<String, Message> received = consumerRecords.poll(10, TimeUnit.SECONDS);

    assertThat(received.value().getText()).isEqualTo(message.getText());
    assertThat(received.value().getTimestamp()).isEqualTo(message.getTimestamp());

  }


  public static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    @Override
    public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
      TestPropertyValues values = TestPropertyValues.of(
          "spring.kafka.producer.bootstrap-servers=" + kafka.getBootstrapServers()
      );
      values.applyTo(configurableApplicationContext);
      LOG.info("connecting test to kafka instance: {}",  kafka.getBootstrapServers());
    }
  }


}
