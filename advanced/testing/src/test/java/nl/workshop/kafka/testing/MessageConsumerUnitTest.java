package nl.workshop.kafka.testing;

import nl.workshop.kafka.testing.consumer.MessageConsumer;
import nl.workshop.kafka.testing.model.Message;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.MockConsumer;
import org.apache.kafka.clients.consumer.OffsetResetStrategy;
import org.apache.kafka.common.KafkaException;
import org.apache.kafka.common.TopicPartition;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;


class MessageConsumerUnitTest {

  private static final String MESSAGE_TOPIC = "messages";
  private static final int PARTITION = 0;

  private MessageConsumer messageConsumer;
  private MockConsumer<String, Message> mockConsumer;

  private List<Message> messages;
  private Throwable pollException;



  @BeforeEach
  void setUp() {
    mockConsumer = new MockConsumer<>(OffsetResetStrategy.EARLIEST);
    messages = new ArrayList<>();
    messageConsumer = new MessageConsumer(mockConsumer, ex -> this.pollException = ex, messages::add);
  }


  @Test
  void verifyMessagesAreConsumedCorrectly() {
    // given
    mockConsumer.schedulePollTask(() -> {
      mockConsumer.rebalance(Collections.singletonList(new TopicPartition(MESSAGE_TOPIC, 0)));
      mockConsumer.addRecord(record(MESSAGE_TOPIC, PARTITION,"key", "test message"));
    });
    mockConsumer.schedulePollTask(() -> messageConsumer.stop());

    HashMap<TopicPartition, Long> startOffsets = new HashMap<>();
    TopicPartition tp = new TopicPartition(MESSAGE_TOPIC, PARTITION);
    startOffsets.put(tp, 0L);
    mockConsumer.updateBeginningOffsets(startOffsets);

    // when
    messageConsumer.consume();

    // then
    assertThat(messages).hasSize(1);
    assertThat(messages.get(0).getText()).isEqualTo("test message");
    assertThat(mockConsumer.closed()).isTrue();
  }


  @Test
  void whenStartingBySubscribingToTopicAndExceptionOccurs_thenExpectExceptionIsHandledCorrectly() {
    // given
    mockConsumer.schedulePollTask(() -> mockConsumer
        .setPollException(new KafkaException("poll exception")));
    mockConsumer.schedulePollTask(() -> messageConsumer.stop());

    HashMap<TopicPartition, Long> startOffsets = new HashMap<>();
    TopicPartition tp = new TopicPartition(MESSAGE_TOPIC, 0);
    startOffsets.put(tp, 0L);
    mockConsumer.updateBeginningOffsets(startOffsets);

    // when
    messageConsumer.consume();

    // then
    assertThat(pollException).isInstanceOf(KafkaException.class).hasMessage("poll exception");
    assertThat(mockConsumer.closed()).isTrue();
  }

  private ConsumerRecord<String, Message> record(String topic, int partition, String key, String message) {
    return new ConsumerRecord<>(topic, partition, 0, key, new Message(message));
  }

}


