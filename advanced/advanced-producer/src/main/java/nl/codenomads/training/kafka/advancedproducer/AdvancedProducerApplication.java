package nl.codenomads.training.kafka.advancedproducer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdvancedProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdvancedProducerApplication.class, args);
	}

}
