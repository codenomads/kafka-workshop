package nl.codenomads.training.kafka.advancedproducer.service;

import nl.codenomads.training.kafka.advancedproducer.model.Player;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.KafkaException;
import org.apache.kafka.common.errors.ProducerFencedException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class PlayerService {

	private String topic;
	private KafkaTemplate<String, Player> template;

	public PlayerService(@Value("${kafka.topic.player}") String topic, KafkaTemplate<String, Player> template) {
		this.topic = topic;
		this.template = template;
	}

	public void sendPlayer(Player player) {
		template.send(topic, player.getId(), player);
	}

	public void sendPlayerTransactional(Player player) {
		Producer<String, Player> producer = template.getProducerFactory().createProducer();

		try{
			producer.beginTransaction();
			producer.send(new ProducerRecord<>(topic, player.getId(), player));
			producer.commitTransaction();
		} catch(ProducerFencedException e) {
			producer.close();
		} catch(KafkaException e) {
			producer.abortTransaction();
		}
	}

	public void sendPlayerTransactionalTemplate(Player player) {
		template.executeInTransaction(template -> {
			template.send(topic, player.getId(), player);

			return true;
		});
	}
}
