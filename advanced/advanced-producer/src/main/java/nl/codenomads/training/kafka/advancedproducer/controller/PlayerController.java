package nl.codenomads.training.kafka.advancedproducer.controller;

import nl.codenomads.training.kafka.advancedproducer.service.PlayerService;
import nl.codenomads.training.kafka.advancedproducer.model.Player;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PlayerController {

	private PlayerService playerService;

	public PlayerController(PlayerService playerService) {
		this.playerService = playerService;
	}

	@PostMapping
	public void sendPlayer(@RequestBody final Player player) {
		playerService.sendPlayer(player);
	}
}
