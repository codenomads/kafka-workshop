package nl.codenomads.training.kafka.advancedproducer.config;

import java.util.List;
import java.util.Map;
import org.apache.kafka.clients.producer.Partitioner;
import org.apache.kafka.common.Cluster;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.utils.Utils;
import org.springframework.stereotype.Component;

@Component
public class PlayerPartitioner implements Partitioner {

	@Override
	public int partition(String topic, Object key, byte[] keyBytes, Object value, byte[] valueBytes, Cluster cluster) {
		List<PartitionInfo> partitions = cluster.partitionsForTopic(topic);
		int numberOfPartitions = partitions.size();

		if ("player1".equals(key)) {
			return numberOfPartitions - 1; // Always in the last partition
		}

		return Math.abs(Utils.murmur2(keyBytes) % (numberOfPartitions - 2)); // Other partitions except for last
	}

	@Override
	public void close() {

	}

	@Override
	public void configure(Map<String, ?> map) {

	}
}
