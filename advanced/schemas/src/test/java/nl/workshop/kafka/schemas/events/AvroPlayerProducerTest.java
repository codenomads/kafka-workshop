package nl.workshop.kafka.schemas.events;

import static org.assertj.core.api.Assertions.assertThat;
import com.fasterxml.jackson.databind.ObjectMapper;


import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import nl.workshop.kafka.avro.schemas.Player;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.kafka.listener.MessageListener;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.utils.ContainerTestUtils;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

// Test with embedded Kafka instance, this annotation gives us the EmbeddedKafkaBroker to autowire
@EmbeddedKafka( partitions = 1, topics = {"notes"})
@ExtendWith(SpringExtension.class)
// set bootstrap-servers property to embedded kafka endpoints
@SpringBootTest(
    properties = "spring.kafka.producer.bootstrap-servers=${spring.embedded.kafka.brokers}"
)
@DirtiesContext
class AvroPlayerProducerTest {

  @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
  @Autowired
  private EmbeddedKafkaBroker embeddedKafkaBroker;

  @Autowired
  private nl.workshop.kafka.schemas.events.AvroPlayerProducer avroPlayerProducer;

  private KafkaMessageListenerContainer<String, Player> container;

  private BlockingQueue<ConsumerRecord<String, Player>> consumerRecords;


  @BeforeEach
  public void setUp() {
    consumerRecords = new LinkedBlockingQueue<>();

    ContainerProperties containerProperties = new ContainerProperties("avro.topic");

    Map<String, Object> consumerProperties = KafkaTestUtils.consumerProps(
        "sender", "false", embeddedKafkaBroker);

    consumerProperties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
    consumerProperties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
    consumerProperties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
    consumerProperties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, KafkaAvroDeserializer.class);
    consumerProperties.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");
    consumerProperties.put(KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG, true);



    DefaultKafkaConsumerFactory<String, Player> consumer = new DefaultKafkaConsumerFactory<>(consumerProperties);




    container = new KafkaMessageListenerContainer<>(consumer, containerProperties);
    container.setupMessageListener((MessageListener<String, Player>) record -> {
      consumerRecords.add(record);
    });
    container.start();

    ContainerTestUtils.waitForAssignment(container, embeddedKafkaBroker.getPartitionsPerTopic());
  }



  @DisplayName("It should send a player to the avro-topic")
  @Test
  void test_sendPlayer() throws InterruptedException {

    Player player = new Player();
    player.setName("chip");
    player.setHighScore(500);
    player.setPicture("https://pixabay.com/vectors/alien-smiley-monster-design-symbol-41604/");

    avroPlayerProducer.sendMessage(player);

    ConsumerRecord<String, Player> received = consumerRecords.poll(10, TimeUnit.SECONDS);

   assertThat(received.value().getName()).isEqualTo(player.getName());
   assertThat(received.value().getHighScore()).isEqualTo(player.getHighScore());
   assertThat(received.value().getPicture()).isEqualTo(player.getPicture());
  }

  @DisplayName("It should send a player without a picture to the avro-topic")
  @Test
  void test_sendPlayerWithoutPicture() throws InterruptedException {

    Player player = new Player();
    player.setName("chip");
    player.setHighScore(500);
    player.setPicture(null);

    avroPlayerProducer.sendMessage(player);

    ConsumerRecord<String, Player> received = consumerRecords.poll(10, TimeUnit.SECONDS);

    assertThat(received.value().getName()).isEqualTo(player.getName());
    assertThat(received.value().getHighScore()).isEqualTo(player.getHighScore());
    assertThat(received.value().getPicture()).isEqualTo(player.getPicture());
  }


  @AfterEach
  public void tearDown() {
    container.stop();
  }

}
