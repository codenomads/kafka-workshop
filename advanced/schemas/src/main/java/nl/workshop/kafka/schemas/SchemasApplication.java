package nl.workshop.kafka.schemas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SchemasApplication {

	public static void main(String[] args) {
		SpringApplication.run(SchemasApplication.class, args);
	}

}
