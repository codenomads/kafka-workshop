# Exercise 3: Producer delivery semantics

In the advanced-producer:

Use the following link as code inspiration:
https://www.baeldung.com/kafka-exactly-once

1. Create an idempotent producer. You do this by setting the idempotency settings in the producer config. Use the https://kafka.apache.org/documentation/#producerconfigs to see which properties need to be set.
2. Write an extra method in the PlayerService that takes a Player object. This method will perform a transaction.
3. Use the KafkaTemplate to get a Kafka Producer.
4. Write the code that starts a transaction, sends a message and commits the transaction. You don't need transactionInit(). 
KafkaTemplate already did that for you. Catch a potential ProducerFencedException and a KafkaException.
5. Create an endpoint in the API to call the transactional method.
6. Before you test look via the CLI tool at the topics. Which topics do you see?
7. Now start the first step of the transaction but wait with sending the record. Look again at the topics in the CLI? Did a new topic appear?
8. Now use the CLI tool to consume from topic "__transaction_state". Although you cannot deserialize the bytes you can see new entries are added as the transaction progresses.
Finish the transaction and check the topic for changes.

9. Now start a second producer with the same transactional id. Halt the first producer mid transaction. For instance after sending but before the commit.
Set a breakpoint at the producer.close() statement in the catch of the ProducerFencedException (in the producer halted mid transaction).
10. Start a transaction in the second producer and finish this transaction.
11. Now continue with the transaction in the first producer. What happens? Why?

12. Create a third method in the player service that takes a player and sends it in a transaction to Kafka.
Do this using the kafkaTemplate directly instead of creating a producer. For tips see 
https://docs.spring.io/spring-kafka/docs/1.3.0.M1/reference/htmlsingle/ on KafkaTemplate section 4.1.2 "KafkaTemplate Local Transactions".