# Exercise 1: Developing a Kafka Producer

In the advanced-producer module

1. Create a config folder
2. Create a configuration class called "KafkaSetupConfig" in the config folder. This class will create the topics. The class needs the following:
    1. @Configuration annotation
    2. An @Autowired field with the KafkaProperties class.
    3. An integer field called "partitions" and an integer field "replicationFactor". Inject these with values from the application.properties file with 6 for the partition and 1 for the replicationFactor.
    4. A bean creating a KafkaAdmin instance in which the AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG in the properties is set equal to the BootStrapServers from the autowired Kafkaproperties
    5. A bean creating a NewTopic using Spring Kafka TopicBuilder. The name of the topic is injected in the beans method name from the application.properties. The partitions and replicationFactor are taken from the fields created in step 3.
    6. This will ensure the topics are created correctly on start up of the application

3. Create a model folder
4. Create a class called "Player" in this folder. The class has the following fields:
    1. String id
    2. String name
    3. int age
    4. Instant created

5. Create class "KafkaProducerConfig" in the configuration folder
6. Again create an @Autowired field for the KafkaProperties class
7. Create a bean that returns a Map<String, Object>. This map contains the producer properties. Set the following properties:
    1. Bootstrap servers
    2. Key serializer so it can handle a String as key
    3. Value serializer so it can handle a Player object as value
8. Create a bean for a ProducerFactory that can send records with a String as the key and a Player as the value and has the properties from step 7.
9. Create a bean for a KafkaTemplate that can send records with a String as the key and a Player as the value using the ProducerFactory from step 8.

10. Create Service class (PlayerService) that has a method that accepts a Player object and uses the KafkaTemplate to send this to Kafka. Use the player id as the key.
11. Create a controller class (PlayerController) that receives a player object from an api call and uses the PlayerService to send this to Kafka
12. Use the Kafka CLI commands that were included to check if the messsage arrived correctly on the topic

13. Implement your own partitioner. This partitioner should partition a player with a certain id to its own partition. No other players can be sent to this partition.
Add the following property to the KafkaProducerSetup props.put(ProducerConfig.PARTITIONER_CLASS_CONFIG, CustomKafkaPartitioner.class.getName()); 
14. Test your partitioner with the CLI tools