# Exercise 2: Developing a Kafka Consumer

In the advanced-consumer module
Links:
Step 1 to 3: https://www.baeldung.com/spring-kafka

1. Create a config folder
2. Copy the class called "KafkaSetupConfig" in the config folder from the producer including the application.properties.
3. Create class called "KafkaConsumerConfig" in the config folder and add the following:
    1. @Configuration annotation
    2. An @Autowired field for KafkaProperties
    3. A bean returning a Map<String, Object> in which the Bootstrap Servers, consumer group id and auto offset reset are copied from the kafkaProperties.
    The Kafka properties are set in the application.properties.
    Add a key and value deserializer to the properties map capable of deserializing a String as key and Player object as value.
    Add trusted packages property "*".
    4. Create a bean for a ConsumerFactory that can consume a message with a String as key and a Player as value. The factory uses the properties from step 3.
    5. Create a bean for a ConcurrentKafkaListenerContainerFactory that can consume a message with a String as key and a Player as value.
    Set the consumer factory of step 4 as the consumer factory of the containerFactory.

4. Create a service class (PlayerConsumer) that has a method to consume a Player message using the @KafkaListener annotation. Log the received message.
5. Start the consumer and test if it consumes a message correctly.
6. Use the CLI commands to see how the partitions are assigned to the consumer.
7. Start two other instances of the consumer application and look again at the partitions.
8. Shut down one of the consumers and look again. Look also at the logs of the Kafka consumer.
9. Now start the consumer with another consumer_group_id. Investigate what happens.