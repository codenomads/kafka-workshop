# Exercise 5: Producer error management

1. Set the retriable error settings in the producer config:
    1. ProducerConfig max in flight requests per second to 1.
    2. ProducerConfig retries to 3
    3. ProducerConfig request timeout 15 seconds (in milliseconds so 15000)
    4. ProducerConfig retry backoff 1 second (in milliseconds so 1_000)


