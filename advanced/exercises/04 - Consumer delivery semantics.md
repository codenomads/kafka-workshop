# Exercise 4: Consumer delivery semantics

1. Start by investigating the Player topic via the CLI tool. Perform a transaction and consume from the topic.
Use kafka-console-consumer.sh and setting isolation-level to switch between read_committed and read_uncommitted. 
This can be done by using the argument --isolation-level=read_committed or --isolation-level=read_uncommitted.
2. Perform one of two transactions in the debugging mode in the producer and look at when the CLI tool displays the messages.

3. Now set the isolation level of the consumer. See https://kafka.apache.org/documentation/#consumerconfigs
4. Switch between isolation levels and produce some messages in transactions while walking through the transactions with the debugger.
 See when the messages arrive at the consumer.
 
 For step 5 use https://www.baeldung.com/kafka-exactly-once as inspiration. Particularly parts 4.4. and 4.6.
 5. Now consume a record and send it to another topic while at the same time committing the offsets in a transaction. You will need the following:
    1. Add a topic to the KafkaSetupConfig. The record will be forwarded to this topic.
    2. Copy the KafkaProducerConfig from the advanced-producer module.
    3. Stop using the listener and inject a consumer in the service.
    3. Consume record in transaction and commit the record and the offset in the same transaction.
    4. Check if the offset and the record have been committed using the CLI tools.