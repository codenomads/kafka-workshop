# Exercise: Implement Producer with Avro Serializer

## Docker schema registry
Setup docker kafka cluster including the schema registry.  
The schema directory contains a ```docker-compose.yml ```
Run ```docker-compose up -d``` this will start a kafka cluster with the schema registry.
Check the supported: http://localhost:8081/schemas/types
Avro should be in the list.

## AVRO
Schemas are composed of primitive types (null, boolean, int, long, float, double, bytes, and string) 
and complex types (record, enum, array, map, union, and fixed).

For more info on Avro data types and schema check:
https://avro.apache.org/docs/1.8.1/spec.html#schemas


This is the schema we are going to implement
```
{
    "namespace": "nl.workshop.kafka.avro.schemas",
    "type": "record",
    "name": "Player",
    "fields": [
        {"name": "name",  "type": "string" },
        {"name": "high_score", "type":"long" },
        {"name": "picture", "type": "string"}
]
}
```

In the schemas module:
1. In the src/main create a folder called avro.
2. Add to the folder created in step 1 a file with the name player.avsc
3. Copy Paste the example schema to the player avsc file.
4. If we run ```mvn verify```  a Player.java file will be generated. In which folder can we find this file? (check in pom.xml)
5. Run ```mvn verify``` and check if the file is generated. We can now use this Player class to configure our Producer. (you might have to do a maven reload project)
6. The producer is build in the AvroPlayerProducer file. Add the key serializer to the Producer properties, it should be a StringSerializer also add the value serializer which should be a KafkaAvroSerializer.
7. Change the Object types in Producer<Object, Object> and  ProducerRecord<Object, Object> to the correct types.
8. Run the tests, after step 7, test_sendPlayer() should succeed
9. test_sendPlayerWithoutPicture() is still failing, we have to change the AVRO schema to pass this test. Make field picture optional in player.avsc.
10. If both test are green, try to add this new field to schema ``` {"name": "email", "type": "string"}```, the tests will fail again.
11. How can you fix the tests?
12. Check http://localhost:8081/subjects/avro.topic-value/versions to see the different versions of the schema.
On http://localhost:8081/subjects/avro.topic-value/versions/1 you should see the first schema version.
 






